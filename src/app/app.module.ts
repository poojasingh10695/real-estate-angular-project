import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { CardSliderComponent } from './card-slider/card-slider.component';
import { DownloadAppComponent } from './download-app/download-app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { TestimonialSliderComponent } from './testimonial-slider/testimonial-slider.component';
import { SearchTabComponent } from './search-tab/search-tab.component';
// import { CdTabsModule } from 'angular-cd-tabs/lib/cd-tabs.module';
import {CdTabsModule} from 'angular-cd-tabs';
import { AboutUsComponent } from './about-us/about-us.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AddPropertyComponent } from './add-property/add-property.component';
import { RehomeComponent } from './rehome/rehome.component';
import { FaqComponent } from './faq/faq.component';
import { PageErrorComponent } from './page-error/page-error.component';
import { PropertyListingComponent } from './property-listing/property-listing.component';
import { BuyPropertyListingComponent } from './buy-property-listing/buy-property-listing.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CardSliderComponent,
    DownloadAppComponent,
    TestimonialSliderComponent,
    SearchTabComponent,
    AboutUsComponent,
    ContactUsComponent,
    AddPropertyComponent,
    RehomeComponent,
    FaqComponent,
    PageErrorComponent,
    PropertyListingComponent,
    BuyPropertyListingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    IvyCarouselModule,
    CdTabsModule,
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
