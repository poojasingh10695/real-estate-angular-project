import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-listing',
  templateUrl: './property-listing.component.html',
  styleUrls: ['./property-listing.component.css']
})
export class PropertyListingComponent implements OnInit {
  cityDetails = [
    {
      src: "../../assets/images/add_property/delhi.png",
      city: 'Delhi / NCR', 
      properties:'162,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/banglore.png",
      city: 'Bangalore', 
      properties:'49,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/chennai.png",
      city: 'Chennai ', 
      properties:'162,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/ahmedabaad.png",
      city: 'Ahmedabad ', 
      properties:'162,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/kolkata.png",
      city: 'Kolkata', 
      properties:'162,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/delhi.png",
      city: 'Delhi / NCR', 
      properties:'162,000+ Properties'
    },
    {
      src: "../../assets/images/add_property/delhi.png",
      city: 'Delhi / NCR', 
      properties:'162,000+ Properties'
    }
    
  ];
  cardDetails = [
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    
  ];
  propertyDetails = [
    {
      src: "../../assets/images/add_property/property1.png",
      title: 'Alessandra Rosales', 
      for:'For Sell',
      price: "₹40,000"
    },
    {
      src: "../../assets/images/add_property/property2.png",
      title: 'Alessandra Rosales', 
      for:'For Sell',
      price: "₹40,000"
    },
    {
      src: "../../assets/images/add_property/property3.png",
      title: 'Alessandra Rosales', 
      for:'For Sell',
      price: "₹40,000"
    },
    
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
