import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddPropertyComponent } from './add-property/add-property.component';
import { BuyPropertyListingComponent } from './buy-property-listing/buy-property-listing.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { HomeComponent } from './home/home.component';
import { PageErrorComponent } from './page-error/page-error.component';
import { PropertyListingComponent } from './property-listing/property-listing.component';
import { RehomeComponent } from './rehome/rehome.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'property-listing', component: PropertyListingComponent },
  { path: 'add-property', component: AddPropertyComponent },
  { path: 'rehome', component: RehomeComponent },
  { path: 'faq', component: FaqComponent },
  { path: '404', component: PageErrorComponent },
  { path: 'buy-property', component: BuyPropertyListingComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
