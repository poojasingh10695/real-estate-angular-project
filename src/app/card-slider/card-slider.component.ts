import { Component, OnInit } from '@angular/core';
import * as data from "../json/data.json";
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-card-slider',
  templateUrl: './card-slider.component.html',
  styleUrls: ['./card-slider.component.css'],

})
export class CardSliderComponent implements OnInit {
  public showContainer: any;
  constructor(public breakpointObserver: BreakpointObserver) { }
  cardDetails = [
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    {
      src: "../../assets/images/home/cardimg.png",
      title: 'Emaar Palm Heights', 
      for:'For Rent',
      location:'Ahmedabad',
      desc: '3 BHK Apartment, Sector-77, Gurgaon', 
      price: "₹40,000"
    },
    
  ];
  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(min-width: 992px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainer = 3.5;
        } else {
          this.showContainer = 1.5;
        }
      });
  }

}
