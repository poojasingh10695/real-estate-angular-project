import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-testimonial-slider',
  templateUrl: './testimonial-slider.component.html',
  styleUrls: ['./testimonial-slider.component.css']
})
export class TestimonialSliderComponent implements OnInit {
  public showContainer: any;

  constructor(public breakpointObserver: BreakpointObserver) { }
  testDetails = [
    {
      src: "../../assets/images/user/user1.png",
      title: 'I felt 110% confident by partnering with Dave', 
      desc: 'I recently sold a house with Dave and while this can be a very stressful process, I felt 110% confident by partnering with Dave. ', 
      username:'Guy Hawkins',
      position:'CEO, Prestige pvt ltd',
      review: "16 Reviews"
    },
    {
      src: "../../assets/images/user/user2.png",
      title: 'I felt 110% confident by partnering with Dave', 
      desc: 'I recently sold a house with Dave and while this can be a very stressful process, I felt 110% confident by partnering with Dave. ', 
      username:'Guy Hawkins',
      position:'CEO, Prestige pvt ltd',
      review: "16 Reviews"
    },
    {
      src: "../../assets/images/user/user1.png",
      title: 'I felt 110% confident by partnering with Dave', 
      desc: 'I recently sold a house with Dave and while this can be a very stressful process, I felt 110% confident by partnering with Dave. ', 
      username:'Guy Hawkins',
      position:'CEO, Prestige pvt ltd',
      review: "16 Reviews"
    },
  ];
  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(min-width: 992px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainer = 350;
        } else {
          this.showContainer = 550;
        }
      });
  }

}
