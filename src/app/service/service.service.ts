import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(public router:Router) { }

  redirectUrl(url:any){
    this.router.navigateByUrl(url);
  }
}
