import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyPropertyListingComponent } from './buy-property-listing.component';

describe('BuyPropertyListingComponent', () => {
  let component: BuyPropertyListingComponent;
  let fixture: ComponentFixture<BuyPropertyListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyPropertyListingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyPropertyListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
